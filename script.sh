alias e1="XDG_CONFIG_HOME=/nvim/.conf_cpp1 nvim"
alias n1="XDG_CONFIG_HOME=/nvim/.conf_cpp3 nvim"
export PATH=/opt1/bin:$PATH
GIT_PS1_DESCRIBE_STYLE="contains"
GIT_PS1_SHOWCOLORHINTS="y"
GIT_PS1_SHOWDIRTYSTATE="y"
GIT_PS1_SHOWSTASHSTATE="y"
GIT_PS1_SHOWUNTRACKEDFILES="y"
GIT_PS1_SHOWUPSTREAM="verbose name git"
PROMPT_COMMAND='__git_ps1 "╭─ \w" "\n╰$(if test $? = 0;then echo "\$";else echo "\[\e[31m\]\$\[\e[0m\]";fi) " " ⎇  %s"'
#PS1="\$(if [[ \$? == 0 ]]; then echo \"\[\033[34m\]\"; else echo \"\[\033[31m\]\"; fi)\342\226\210\342\226\210 [ \w ] \$(__git_ps1 '(%s)') \n\[\033[m\]\342\226\210\342\226\210 "
